var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var fs = require('fs');
var imgPath = './images/uploadSnap.png';


var DemoSchema = new Schema({
 	firstName : String,
     middleName: String,
     lastName : String,
     gender : String,
     age : Number,
     email : String,
     contactNumber : Number,
     alternateNumber : Number,
     qualification : String,
     height: Number,
     bloodGroup : String,
     annualIncome : String,
     jobBusiness : String,
     firstGotra : String,
     seconGotra : String,
     maternalUncleName : String,
     fatherName: String,
     motherName : String,
     address : String,
     anubandhid : String,
     contactAddress : String,
     contactPersonNumber : Number,
     uploadSnap: { data: Buffer, contentType: String }
     
});



module.exports = mongoose.model('Demo', DemoSchema);
